\mainpage

<img src="Confused_man.jpg" width="300" height="300" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<span style="font-size:170%;">
<strong>Is this PTVR really for me ? </strong>
</span>
<BR><BR><BR><BR><BR><BR><BR>
(image from <a href="https://commons.wikimedia.org/wiki/File:Confused_man.jpg">Notas de prensa</a>, <a href="https://creativecommons.org/licenses/by-sa/2.5">CC BY-SA 2.5</a>, via Wikimedia Commons).


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

# Overview of PTVR #


PTVR is a free and open-source library for creating visual perception experiments in virtual reality using high-level Python script programming.

The PTVR library's ambition is to let you design and implement visual perception experiments in virtual reality using high-level Python script programming. It is intended to help researchers in behavioral sciences leverage the power of virtual reality for their research without the need to learn how virtual reality programming works. This library will be open source and released soon. 

The philosophy of PTVR is thus very close to the approach of PsychoPy (https://www.psychopy.org/) that has been so important and influential since 2007 for Vision Science researchers displaying stimuli on 2D monitors.

##Some of PTVR assets are:
-     VR experiments can be created rapidly and succinctly by researchers thanks to clearly structured high-level python scripts.
-     The use of such scripts, along with the transparency of the PTVR open-source code, is one of the necessary components to address the 'reproducibility crisis' in science and foster Open Science.
-     Researchers can learn PTVR quite quickly thanks to two main resources:
	-     A rich online documentation provided with many animated figures in 3D to help visualize some 3D subtleties (will be released soon).
	- Many "demo" scripts (included in the PTVR library) with didactic 3D and/or interactive features.
-     The terminology used in the PTVR code is familiar to vision scientist. For instance, researchers  can use an intuitive "Perimetric" coordinate system allowing them to easily place their 3D stimuli with eccentricity and half-meridian coordinates.
-     Intuitive ways of dealing with visual angles in 3D.
-     Intuitive ways of dealing with the notorious difficulty of rotating 3D objects.
-     An implementation of "flat screens" to replicate and extend standard experiments made on 2D screens of monitors.
-     Easy ways of recording experimental results.
-     Easy Gaze tracking and recording.
-     Easy Head tracking and recording.
-     Focus on the implementation of standard and innovative clinical tools for visuo-motor testing and visuo-motor readaptation (notably for low vision).
-     Great care to create accurately controlled text stimuli in the spirit of the psychophysics of Reading.


##Requirements

-     An HTC Vive Pro Series VR system (headset, hand controllers, ...)  (https://www.vive.com/eu/product/#pro%20series)
-     A Windows computer with HTC Vive already set-up. See HTC Vive setup for help with this.
-     Python >3.6. We suggest Anaconda, a nice Python distribution, and Spyder, a nice integrated development environment (IDE) already included in Anaconda.

Future PTVR versions will support other VR systems.


##Notes

-     PTVR is built using the Unity game engine (you will however not have to install Unity yourself).
-     The PTVR project folder can easily be opened as a Unity project (you will however not have to install Unity yourself).


<!-- DEBUT DE texte enlevé 
## Notes de Helen
EC: il y a un pb à régler: 
- les sections s'affichent dans "Getting started with PTVR"

- PB 2: si on écrit qch derrière slash-Mainpage (ci-dessus) ça crée une nouvelle section (et donc une indentation) dans Documentation for users.

Perception Toolbox for Virtual Reality (PTVR) is a software platform allowing researchers to create VR experiments based on Unity without any knowledge of Unity: experiments are simply written with scripts in Python. 


conseils de Helen pour chercher l'inspiration.  
[https://neurokit2.readthedocs.io/en/latest/introduction.html](https://neurokit2.readthedocs.io/en/latest/introduction.html)  
[https://docs.docker.com/get-started/overview/](https://docs.docker.com/get-started/overview/)  
[http://docs.juliaplots.org/latest/](http://docs.juliaplots.org/latest/)

ET s'inspirer bien sûr de ce qu'on fait déjà dans le site web (et noter qu'on vient de notre Site Web quand on arrive ici).


Goal of PTVR
--
xxx

**FAIRE un lien vers Psychophy pour illustrer le fait qu'on partage leur philosophie de l'importance des scripts etc... en sciences expérimentales**

QQS exemples  de code + captures d'écran
--
genre: 
la manip visual search
MNREAD


Ecosystem
--
public de PTVR 
expliquer ce que va pouvoir faire l'utilisateur,
expliquer que c'est pour différents types de users:
- researcher
- operator
- contributor: PTVR developer or Documentation editing


FIN DE texte enlevé -->


Subsections of the Documentation for users
--

 -   @subpage gettingStartedwithPTVR
<BR>
 -   @subpage BehaviouralSciences
<BR>
 -   @subpage UserManual
<BR>
 -   @subpage FamousExperiments
<BR>
 -   @subpage Index
<BR>
 -   @subpage Glossary
<BR>
 -   @subpage FAQ  
<!--
<BR>
 -   @subpage Contribute
-->
<BR>
<BR>


